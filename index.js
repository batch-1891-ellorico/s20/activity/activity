

// NUMBER LOOP

// #3
let number = Number(prompt('Please input number:'))
console.log(`The number you provided is: ${number}`)

// #4
for (let i = number; i >= 0; i--) {

// #6 
	if (i % 10 == 0 ) {
		console.log('The number is divisible by 10. Skipping the number')
		continue;
	}

// #7
	if (i % 5 == 0) {
		console.log(i)
		continue;
	}

// #5
	if (i <= 50) {
		console.log ('The current value is at 50. Terminating the loop.')
		break;
	}
}

// STRING LOOP

// #8
let word = "supercalifragilisticexpialidocious"
console.log(word)

// #9
let consonants;
console.log(word.length)

// #10
for (let i = 0; i <= word.length; i++){
	if(  
		word[i] == "a" ||
		word[i] == "e" ||
		word[i] == "i" ||
		word[i] == "o" ||
		word[i] == "u"	) {

	} else {
		console.log(word[i])
	}
}